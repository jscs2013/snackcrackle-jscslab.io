function snapCrackle(maxValue){
    let result=''      
    for(let i=1; i<=maxValue; i++){
        if (i==maxValue){
            if (i%2!=0 && i%5==0){
              result+='SnapCrackle.'
            } else if (i%5==0){
              result+='Crackle.'
            } else if (i%2!=0) {
                result+='Snap.'
            } else {
                result+=i +'.'
            }
        } 
        else if (i%2!=0 && i%5==0){
        result+='SnapCrackle '
      } else if (i%5==0){
        result+='Crackle, '
      } else if (i%2!=0) {
        result+='Snap, '
      } else {
          result+=i+', '
      }
    }
  console.log(result);
  return result
}


function snapCracklePrime(maxValue) {
    let result=''
    for(let i=1; i<=maxValue; i++) {
      if (i==maxValue) {
        let counter=[]
        for (let div=1; div<i; div++) {
          if (i%div==0) {
            counter.push(div)
          }
        } 
        if (counter.length<2) {
          if (i%2!=0 && i%5==0) {
            result+='SnapCracklePrime.'
          }
          else if (i%5==0) {
            result+='CracklePrime.'
          }
          else if (i%2!=0) {
            result+='SnapPrime.' 
          }
          else {
            result+='Prime.'
          }
        }
        else if (i%2!=0 && i%5==0) {
          result+='SnapCrackle.'
        } 
        else if (i%5==0) {
          result+='Crackle.'
        } 
        else if (i%2!=0) {
          result+='Snap.'
        } 
        else {
          result+=i +'.'
        }
      } 
      else if (i>=2) {
        let counter1=[]
        for (let div1=2; div1<=i; div1++){
          if (i%div1==0){
            counter1.push(div1)
          }
        } 
        if (counter1.length<2) {      
          if (i%2!=0 && i%5==0) {
            result+='SnapCracklePrime, '
          }
          else if (i%5==0) {
            result+='CracklePrime, '
          }
          else if (i%2!=0) {
            result+='SnapPrime, ' 
          }
          else {
            result+='Prime, '
          }
        } else if (i%2!=0 && i%5==0) {
          result+='SnapCrackle, '
        } 
        else if (i%5==0) {
          result+='Crackle, '
        } 
        else if (i%2!=0) {
          result+='Snap, '
        }
        else {
          result+=i +', '
        }
      }
      else if (i%2!=0 && i%5==0) {
        result+='SnapCrackle, '
      }
      else if (i%5==0) {
        result+='Crackle, '
      } 
      else if (i%2!=0) {
        result+='Snap, '
      } else {
        result+=i+', '
      }
    }
  console.log(result)
  return (result)
}